Document: gtypist-manual-es
Title: Manual de GNU Typist
Author: GNU Typist Development Team <bug-gtypist@gnu.org>
Abstract: GNU Typist es un tutor interactivo de mecanografiado que
 ayuda a escribir correctamente. Tiene varias lecciones para
 diversas disposiciones de teclado y en diferentes idiomas.
 Las lecciones de gtypist se describen en un lenguaje de script
 de fácil aprendizaje que el usuario puede emplear para modificar
 las lecciones existentes o crear nuevas. 
Section: Education

Format: HTML
Index: /usr/share/doc/gtypist/gtypist.es.html
Files: /usr/share/doc/gtypist/gtypist.es.html
