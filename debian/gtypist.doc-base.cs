Document: gtypist-manual-cs
Title: Příručka pro GTypist
Author: GNU Typist Development Team <bug-gtypist@gnu.org>
Abstract: GNU Typist je interaktivní výukový program, který vám pomůže
 osvojit si principy správného psaní na klávesnici všemi deseti prsty.
 Obsahuje několik lekcí pro různé typy klávesnic a různé jazyky. (Mezi
 nimi nechybí ani čeština.) Lekce pro gtypist jsou napsány ve snadno
 pochopitelném skriptovacím jazyku, takže je mohou uživatelé modifkovat
 či vytvářet nové. Pokud zadáte parametr cs.typ, spustíte lekce v
 češtině zaměřené na českou klávesnici.
Section: Education

Format: HTML
Index: /usr/share/doc/gtypist/gtypist.cs.html
Files: /usr/share/doc/gtypist/gtypist.cs.html
